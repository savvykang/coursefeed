<?php

require_once("coursefeed.php");
require_once("template/category.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

if ($_SERVER['REQUEST_METHOD'] == "GET") {
	startblock('content');
    template\category\renderInsertCategoryForm();
    endblock('content');
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (http\has_parameter($_POST, "name")) {
        $name = $_POST["name"];
        $coursefeed->insertCategory($name);
    }    
}

?>
