<?php
require_once("coursefeed.php");
require_once("template/adminuser.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php');
startblock('content');


if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (http\has_parameter($_GET, "userid")) {
        $id = $_GET["userid"];
        if($_GET['delorch']=="delete"){    
            template\adminuser\askDelete($coursefeed->getUser($id));
        }
        else{
        //change level
            template\adminuser\levelChange($coursefeed->getUser($id));
            
        }
    }
    
}
 endblock('content');
?>
