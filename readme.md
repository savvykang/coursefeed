CourseFeed
==========

CSE326 Project.

Preview
-------
Go [CourseFeed](http://coursefeed.herokuapp.com).


Installation
------------
1. Create the DB (for example, `coursefeed`).

2. Type your DB credential to `credential.json`.
> {
>     "host": "localhost",
>     "db": "coursefeed",
>     "user": "root",
>     "pass": "password"
> }

3. Go `<php server>/<coursefeed path>/setup.php`.


Dependency Notice
-----------------

- Font Awesome
The Font Awesome font is licensed under the SIL OFL 1.1:
http://scripts.sil.org/OFL
Font Awesome CSS, LESS, and SASS files are licensed under the MIT License:
http://opensource.org/licenses/mit-license.html
Font Awesome by Dave Gandy - http://fontawesome.io


- php-ti
php-ti is released under the MIT License.
https://github.com/arshaw/phpti/blob/master/license.txt
Copyright (c) 2009 Adam Shaw


- Pure
Pure is free to use under the Yahoo! Inc. BSD license.
https://github.com/yui/pure/blob/master/LICENSE.md
Copyright 2013 Yahoo! Inc. All rights reserved.


- Prototype.js
Prototype is Copyright © 2005-2007 Sam Stephenson. It is freely distributable under the terms of an MIT-style license.
http://prototypejs.org/license.html


- Scriptaculous
script.aculo.us was created by Thomas Fuchs, and is extended and improved by open-source contributors. script.aculo.us is released under the MIT license.
http://madrobby.github.io/scriptaculous/


- PHPMailer
PHPMailer is licenced under the LGPL 2.1.
https://github.com/PHPMailer/PHPMailer/blob/master/LICENSE
