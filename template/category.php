<?php

namespace template\category;

?>

<? function renderInsertCategoryForm() { ?>
	<h2>Add New Category</h2>
    <form method="post" class="pure-form pure-form-stacked">
	    <div><label>Name<input name="name" type="text" /></label></div>
	    <div><input class="pure-button pure-button-primary" type="submit" value="Add Category" /> | <a href="article.php">list</a></div>
	</form>
<? } ?>
