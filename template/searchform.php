<?php

namespace template\searchform;

?>

<? function renderSearchForm() { ?>
    <form class="pure-form search-form pure-form-stacked">
    <label>Course Search <input type="text" id="autocomplete" name="autocomplete_parameter" />
    </label>
    </form>

    <div id="autocomplete_choices" class="autocomplete" style="display: none"></div>
    <div id="course_spans"></div>

    <form class="pure-form" id="searchform" method="get" action="article.php">
        <input id="course_id" name="course_id" type="hidden" />
        <label for="coursesearch" class="pure-button"><i class="fa fa-search"></i> Search</label>
        <input type="submit" id="coursesearch" value="Search!"/>
    </form>
<? } ?>
